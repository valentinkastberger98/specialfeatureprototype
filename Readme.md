## Special Feature prototype


This is a simple prototype illustrating the usage of the openai for javascript.
It can generate n QnA-type Cards in a json format (adaptable) for a specific topic on a specific level

The code is largely based on:
https://javascript.plainenglish.io/getting-started-with-openai-api-in-javascript-d1bc365069f0

## How to run:

0. run: npm i

1. Generate an OpenAI API_KEY or send me your e-mail address on discord and i will send you an invite. From there on you can generate your own API_KEY or use an already created one.
⚠️ I think you have to be a paid user to actually use the api

2. Create a .env File in the root of this project and enter
OPENAI_KEY='YOUR_API_KEY'

3. Adapt the generation of the openapi request in index.js to your liking

4. Hit that run button

## Findings:

- Pricing: index.js contains a MAX_TOKEN attribute which defines the maximum number of tokens the request can have
- Tokens are kinda weird and 1 Token equals roughly 4-Characters, 1 Token equals roughly 0,0002$.
- I have set the max monthly payment limit of my account to 5$ (let's see how far this will get us)