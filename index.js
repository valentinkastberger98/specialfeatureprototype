import { OpenAI } from "./src/openai/OpenAI.js";
import dotenv from 'dotenv';

dotenv.config();
// Creating a new instance of the OpenAI class and passing in the OPENAI_KEY environment variable
const openAI = new OpenAI(process.env.OPENAI_KEY);
const topic = 'French Revolution';
const ammount = 3;
const level = 'High School'
const model = 'text-davinci-003';
const format = '[{question: string, answer: string}]';
const MAX_TOKENS = 500;
// Function to generate the prompt for the OpenAI API 
const generatePrompt = (ammount,topic,level) => {
    return `Write "${ammount}" Questions about "${topic}" on a "${level}"-level in this format: "${format}".`
};

// Use the generateText method to generate text from the OpenAI API and passing the generated prompt, the model and max token value
await openAI.generateText(generatePrompt(ammount,topic,level,format), model, MAX_TOKENS)
    .then(text => {
        console.log(text);
    })
    .catch(error => {
        console.error(error);
    });